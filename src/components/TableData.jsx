import React from 'react'
import { Container, Table, Button } from 'react-bootstrap'


function TableData(props) {
    return (
        <Container>
            <Button
                variant="info"
                onClick={() => {
                    
                }}
            >Reset</Button>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Food</th>
                        <th>Amount</th>
                        <th>Price</th>
                        <th>Qty</th>
                    </tr>
                </thead>
                <tbody>{
                    props.items.map((list, i) => (
                        <tr key={i}>
                            {
                                <td>{list.id}</td>
                            }
                            {
                                <td>{list.title}</td>
                            }
                            {
                                <td>{list.amount}</td>
                            }
                            {
                                <td>{list.total}</td>
                            }
                            {
                                <td>{list.total}</td>
                            }
                        </tr>
                    ))}

                </tbody>
            </Table>
        </Container>
    )
}

export default TableData
